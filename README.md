# slides-template

This README will give you the basics on how implement your own Reveal JS slides static gitlab page.

## Make your own Reveal JS slides

1. You will need developer privaledges to create a lesson repo in the [90cos/public/training/lessons](https://gitlab.com/90cos/public/training/lessons) subgroup
   1. If you want to make a subgroup in Lessons, contact a maintainer
   2. For additional privileges, contact a maintainer
2. Fork the template repo into the ```90cos/public/training/lessons``` subgroup
3. Navigate to the newly created repository
   1. Go to ```Settings > General``` and change the Project Name and Project Description
   2. In ```Settings > General > Advance``` change the change path of the repo to one representative of the project name
5. Clone the repo on your favorite dev workstation or use the Gitlab Web IDE
   1. Fill will content
   2. Push to Gitlab
6. Push and WIN!
NOTE: After you push the pipeline will run. Once finished, you will be able to go to ```Settings > Pages``` so find the URL

## Creating content for Reveal JS

Make sure to look at the [Basic HTML Cheatsheet](https://htmlcheatsheet.com/) for a syntax refresher.

1. The entirety of the slides will be defined inside the 'index.html' file
2. Every slide is defined by the ```<section></section>``` tag
   1. These tags should be present inside the ```<div class="slides">``` container
   2. You can also nest ```<section></section>``` tags to create virtical slides. See example below.
3. All slides should have a ```<h2>``` header
4. You can have ```<ul></ul>``` unordered lists or ```<ol></ol>``` ordered lists (numbers)
   1. You can have nested lists, as well. Just make sure you do not overflow the view container. Push your content and view it.
   2. If you have single item lists, plese consider reformatting them or making them their own point.
5. You can also put images with the ```<img src="">``` tag
   1. create a folder and place you images inside of it.
   2. the src="" will have the location of the image.
   3. Example, in an images folder you have test.png. You would put src="images/test.png"
6. Instructor notes are a thing. You can wrap text in a ```<aside class="notes"></aside>``` tag. Use the 'S' hotkey to access instructor view. See example below.
7. Follow the progression layout in the template. Title -> summary -> CONTENT -> conclusion -> demo (optional) -> questions
8. Push and WIN!

### Example

```html
<div class="reveal">
   <div class="slides">
      <section data-background-image="images/org_symbol.png" data-background-size="13%" data-background-position="top 5% left 5%">
         <h1>Template Slides!</h1>
         <h4>90th Cyberspace Operations Squadron</h4>				
      </section>
      <section data-background-image="images/org_symbol.png" data-background-size="13%" data-background-position="top 5% left 5%">
         <h2>Summary</h2>
         <ul>
            <li>What is it?</li>
            <li>Why would we use it?</li>
            <li>Installation</li>
            <li>Basic Implementation</li>
            <li>Final Thoughts</li>
            <li>Demo</li>
         </ul>
         <aside class="notes">
            Put your instructor notes in these 'aside' sections. Press 'F' for full screen! Press 'S' for instructor view!!
         </aside>
      </section>

      <!-- FILL WITH SECTIONS (SLIDES) -->
      <section>
         <section>
            <h2>Basic hotkeys</h2>
            <ul>
               <li>Press 'F' for full screen!</li>
               <li>Press 'S' for instructor view!!</li>
               <li>Press ESC for overview!!!</li>
            </ul>
         </section>
         <section>
            <h2>Virtical slide example</h2>
         </section>
         <section>
            <h2>Another Virtical slide example</h2>
         </section>
      </section>

      <section data-background-image="images/org_symbol.png" data-background-size="13%" data-background-position="top 5% left 5%">
         <h2>Conclusion</h2>
         <ul>
            <li>What is it?</li>
            <li>Why would we use it?</li>
            <li>Installation</li>
            <li>Basic Implementation</li>
            <li>Final Thoughts</li>
            <li>Demo</li>
         </ul>
      </section>
      <section>
         <h3>Demo</h3>
      </section>
      <section data-background-image="images/org_symbol.png" data-background-size="13%" data-background-position="top 5% left 5%">
         <h2>Questions?</h2>
      </section>
   </div>
</div>
```

## More information

You can look at the Reveal JS repository [here](https://github.com/hakimel/reveal.js/). You can also look at the tutorial slides [here](https://revealjs.com/). NOTE: this link is blocked on NIPR

## Questions or Problems

Make an Issue ticket on this repo